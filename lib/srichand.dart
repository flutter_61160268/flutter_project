import 'package:cosmetics_for_your/srichand1_detail.dart';
import 'package:flutter/material.dart';

class Srichand extends StatelessWidget {
  const Srichand({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แป้งพัฟศรีจันทร์'),
      ),
      body: ListView(
        children: [
          Image.asset("assets/images/srichand1.jpg",
              width: 150, height: 300, fit: BoxFit.contain),
          Srichand_more(),
        ],
      ),
    );
  }
}

class Srichand_more extends StatelessWidget {
  const Srichand_more({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: TextButton(
                      child: Text(
                        "srichand translucent compact powder".toUpperCase(),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Srichand1()),
                        );
                      },
                    )),
                Text(
                  'แป้งผสมรองพื้น',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
                Text(
                  'ราคา 259 บาท',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                      fontSize: 20),
                ),
              ],
            ),
          ),
          /*3*/
          Icon(
            Icons.groups_sharp,
            color: Colors.orange[900],
          ),
          Icon(
            Icons.favorite,
            color: Colors.pinkAccent,
          ),
        ],
      ),
    );
  }
}
