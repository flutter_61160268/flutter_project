import 'package:cosmetics_for_your/cutepress.dart';
import 'package:cosmetics_for_your/home_page.dart';
import 'package:cosmetics_for_your/srichand.dart';
import 'package:flutter/material.dart';

class Showlist extends StatelessWidget {
  const Showlist({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Main();
  }
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.exit_to_app),
              color: Colors.white,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomeScreen()));
              })
        ],
      ),
      body: ListView(
        children: [
          Image.asset(
            "assets/images/mabelline-fit-me-new.jpg",
            width: 600,
            height: 300,
            fit: BoxFit.contain,
          ),
          titleSection,
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Center(child: Text('Menu')),
              decoration: BoxDecoration(
                color: Colors.teal,
              ),
            ),
            ExpansionTile(
              title: Text('แป้งพัฟ'),
              children: <Widget>[
                ListTile(
                  title: Text('แป้งพัฟศรีจันทร์'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Srichand()),
                    );
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
                ListTile(
                  title: Text('แป้งพัฟคิวเพรส'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Cutepress()),
                    );
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
            ExpansionTile(
              title: Text('อายแชโดว์'),
              children: <Widget>[
                ListTile(
                  title: Text('Etude'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/eyeshadow');
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
                ListTile(
                  title: Text('BrowIt'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/eyeshadow');
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
            ExpansionTile(
              title: Text('ดินสอเขียนคิ้วและมาสคาร่า'),
              children: <Widget>[
                ListTile(
                  title: Text('มาสค่ารามิสทิน'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/eyebrowandmaskara');
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
                ListTile(
                  title: Text('ดินสอเขียวคิ้ว odbo'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/peyebrowandmaskara');
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
            ExpansionTile(
              title: Text('ลิปสติก'),
              children: <Widget>[
                ListTile(
                  title: Text('Sasi'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/eyebrowandmaskara');
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
                ListTile(
                  title: Text('Sinanna'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/maskara');
                  },
                  trailing: Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Maybelline Fit Me Matte+Poreless '
                ' Powder 12H SPF28 PA+++ 6 g.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'แป้งคุมมัน ปกปิดเรียบเนียน เนื้อแมท',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
            Text(
              'ราคา 199 บาท',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.red, fontSize: 20),
            ),
          ],
        ),
      ),
      /*3*/
      Icon(
        Icons.groups_sharp,
        color: Colors.orange[900],
      ),
      Icon(
        Icons.favorite,
        color: Colors.pinkAccent,
      ),
    ],
  ),
);






  //  theme: ThemeData(
  //       primarySwatch: Colors.teal,
  //     ),
  //     initialRoute: '/',
  //     routes: {
  //       '/': (BuildContext context) => const Showlist(),
  //       '/srichand': (BuildContext context) => const Srichand(),
  //       '/cutepress': (BuildContext context) => const Cutepress()
  //     },