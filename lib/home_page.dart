import 'dart:js';

import 'package:cosmetics_for_your/showlist.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  get child => null;

  Widget buildBtn(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 100),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Cosmetics",
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 80,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Caveat'),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "For Your",
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 50,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Caveat'),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: TextButton(
                    child: Text("เข้าสู่ระบบ".toUpperCase(),
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w800)),
                    style: ButtonStyle(
                        padding: MaterialStateProperty.all<EdgeInsets>(
                            EdgeInsets.only(
                                left: 100, bottom: 22, top: 22, right: 100)),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.yellow),
                        foregroundColor: MaterialStateProperty.all<Color>(
                            Colors.blue.shade800),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(18.0),
                                    side: BorderSide(color: Colors.white)))),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Showlist()),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: GestureDetector(
            child: Stack(
          fit: StackFit.expand,
          children: [
            Image(
              image: AssetImage("assets/images/background.jpg"),
              alignment: Alignment.center,
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            buildBtn(context)
          ],
        )),
      ),
    );
  }
}

Widget titleSection1 = Container(
  child: Row(
    children: <Widget>[
      Expanded(
          child: Text(
        ' COSMETICS '
        ' FOR YOUR ',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 50.0,
            color: Colors.black,
            fontFamily: 'Caveat',
            fontWeight: FontWeight.w800),
      ))
    ],
  ),
);
