import 'dart:html';
import 'package:cosmetics_for_your/cutepress.dart';
import 'package:cosmetics_for_your/home_page.dart';
import 'package:cosmetics_for_your/showlist.dart';
import 'package:cosmetics_for_your/srichand.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to MyShop',
      home: HomeScreen(),
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
    );
  }
}
