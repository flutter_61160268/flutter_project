import 'package:flutter/material.dart';

class Cutepress extends StatelessWidget {
  const Cutepress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แป้งพัฟคิวเพรส'),
      ),
      body: ListView(
        children: [
          Container(
            child: Column(
              children: [
                Image.asset("assets/images/CP2.jpg",
                    width: 500, height: 350, fit: BoxFit.contain),
              ],
            ),
          ),
          titleSection,
        ],
      ),
    );
  }
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Cute Press Evory Retouch Oil Control '
                ' Foundation Powder SPF 30 PA+++ ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'คุมมัน กันน้ำกันเหงื่อ',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
            Text(
              'ราคา 189 บาท',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.red, fontSize: 20),
            ),
          ],
        ),
      ),
      /*3*/
      Icon(
        Icons.groups_sharp,
        color: Colors.orange[900],
      ),
      Icon(
        Icons.favorite,
        color: Colors.pinkAccent,
      ),
    ],
  ),
);
