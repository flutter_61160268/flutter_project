import 'package:flutter/material.dart';

class Srichand extends StatelessWidget {
  const Srichand({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('รองพื้น'),
      ),
      body: ListView(
        children: [
          Image.asset('images/may2.jpg',
              width: 200, height: 60, fit: BoxFit.cover),
          titleSection,
        ],
      ),
    );
  }
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'รองพื้น Maybelline Fit Me Matte + Poreless '
                ' Liquid Foundation 18 ml. ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'รองพื้นแบบหลอด',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
            Text(
              'ราคา 289 บาท',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.red, fontSize: 20),
            ),
          ],
        ),
      ),
      /*3*/
      Icon(
        Icons.groups_sharp,
        color: Colors.orange[900],
      ),
      Icon(
        Icons.favorite,
        color: Colors.pinkAccent,
      ),
    ],
  ),
);
