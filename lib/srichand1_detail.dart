import 'package:flutter/material.dart';

class Srichand1 extends StatelessWidget {
  const Srichand1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('แป้งพัฟศรีจันทร์'),
      ),
      body: ListView(
        children: [
          Container(
            child: Row(
              children: [
                Image.asset("assets/images/SC2.jpg",
                    width: 500, height: 350, fit: BoxFit.contain),
                // Image.asset("assets/images/srichand-perfecting.jpg",
                //     width: 500, height: 350, fit: BoxFit.contain),
              ],
            ),
          ),
          titleSection1,
        ],
      ),
    );
  }
}

Widget titleSection1 = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        /*1*/
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /*2*/
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Srichand1 luminescence Glowing Brilliance Perfecting Powder',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Text(
              'แป้งผสมรองพื้น',
              style: TextStyle(
                color: Colors.grey[500],
              ),
            ),
            Text(
              'ราคา 259 บาท\n',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.red, fontSize: 20),
            ),
            Text(
              'แป้งอัดแข็งผสมรองพื้นสูตรพิเศษที่มีเนื้อละเอียดละมุน',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              '- ช่วยมอบความเนียนนวลให้กับผิวหน้าพร้อมการปกปิดผิวระดับปานกลาง\n'
              '- ช่วยให้ผิวของคุณแลดูเปล่งประกายอย่างเป็นธรรมชาติตลอดวัน\n'
              '- แป้งผสมรองพื้นสูตรควบคุมความมันที่เหมาะสำหรับผู้หญิงเอเชียที่มีผิวมันหรือผิวผสม\n'
              '- พัฟเนื้อดีช่วยให้การทาและเกลี่ยอณูแป้งให้กระจายตัวเกาะติดผิวหน้าของคุณได้ดียิ่งขึ้น SPF 20 PA+++ \n'
              '- ปราศจากน้ำหอมและ พาราเบน ผ่านการทดสอบการระคายเคือง',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'มี 3 เฉดสี',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              '- SC10 Ivory สำหรับผิวขาวหรือขาวเหลือง\n'
              'SC30 Warm Beige สำหรับผิวสองสี\n'
              'SC50 Honey สำหรับผิวแทนหรือผิวเข้ม\n',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      /*3*/
      Icon(
        Icons.groups_sharp,
        color: Colors.orange[900],
      ),
      Icon(
        Icons.favorite,
        color: Colors.pinkAccent,
      ),
    ],
  ),
);

// Widget titleSection2 = Container(
//   padding: const EdgeInsets.all(32),
//   child: Row(
//     children: [
//       Expanded(
//         /*1*/
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             /*2*/
//             Container(
//               padding: const EdgeInsets.only(bottom: 8),
//               child: const Text(
//                 'แป้งอัดแข็งผสมรองพื้นสูตรพิเศษที่มีเนื้อละเอียดละมุน',
//                 style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ],
//   ),
// );
